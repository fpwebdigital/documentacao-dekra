*****************************
Documentação Dekra - Overview
*****************************


Esse documento servirá de base para a equipe Técnica fazer os acessos e as requisições via API no sistema IBM Campaign Automation.

Overview da API do Watson Campaign Automation: `https://developer.ibm.com/customer-engagement/docs/watson-marketing/ibm-engage-2/watson-campaign-automation-platform/xml-api/api-xml-overview/ <https://developer.ibm.com/customer-engagement/docs/watson-marketing/ibm-engage-2/watson-campaign-automation-platform/xml-api/api-xml-overview/>`_.

Login de Usuário
----------------

Antes de chamar qualquer operação, você deve obter um ID de sessão usando a operação Login como mostrado abaixo.

Deverá fazer uma requisição ``POST`` no seguinte endereço: http://api9.silverpop.com/XMLAPI

Feito isso precisa passar os parâmetros abaixo para receber de volta o ``SESSIONID``  que será usado para fazer a conexão da API.

Sintaxe Usando XML para conexão
-------------------------------

.. code-block:: html

        <Envelope>
          <Body>
            <Login>
              <USERNAME>usuario WCA</USERNAME>
              <PASSWORD>senha de acesso ao WCA</PASSWORD>
            </Login>
          </Body>
        </Envelope>


Retorno Aguardado

.. code-block:: html

        <Envelope>
             <Body>
                <RESULT>
                    <SUCCESS>true</SUCCESS>
                    <SESSIONID>18CB5F81EA52FF880DC49514EA7BEC97</SESSIONID>
                    <ORGANIZATION_ID>1736d872-16548c1aefd-aeff503c2fed6201e450c25df8cdcd05</ORGANIZATION_ID>
                    <SESSION_ENCODING>;jsessionid=18CB5F81EA52FF880DC49514EA7BEC97</SESSION_ENCODING>
                </RESULT>
            </Body>
        </Envelope>


Com o retorno TRUE você usará o SESSIONID na URL pra fazer a conexão da seguinte forma:

http://api1.silverpop.com/XMLAPI?jsessionid=18CB5F81EA52FF880DC49514EA7BEC97




Logout de Usuário
-----------------

Depois de concluir as ações da API, você deve usar uma solicitação de Logout para fechar e invalidar a sessão, conforme mostrado abaixo.

Logout

.. code-block:: html

        <Envelope>
          <Body>
            <Logout/>
          </Body>
        </Envelope>


Retorno Aguardado

.. code-block:: html

        <Envelope>
            <Body>
                <RESULT>
                    <SUCCESS>TRUE</SUCCESS>
                </RESULT>
            </Body>
        </Envelope>


.. _inserir-e-atualizar-dados-em-uma-tabela-relacional:


Tabela Relacional - Overview
----------------------------

Documentação: https://developer.ibm.com/customer-engagement/tutorials/insert-update-records-relational-table/

.. note::
        Apenas 100 linhas podem ser passadas em uma única chamada InsertUpdateRelationalTable.


ID da Tabela Relacional: ``11072``



Listando o conteúdo da Tabela Relacional
----------------------------------------

.. code-block:: html

        <Envelope>
          <Body>
            <GetListMetaData>
              <LIST_ID>11009</LIST_ID>
            </GetListMetaData>
          </Body>
        </Envelope>


Estrutura da tabela relacional
------------------------------

- ``TABLE_ID:`` O ID da Tabela de Automação de Campanha do Watson que deve ser atualizada.
- ``ROWS:`` Nó XML que contém elementos ROW para cada linha que está sendo inserida ou atualizada.
- ``ROW:`` Nós XML que definem uma linha de tabela relacional e suas colunas.
- ``COLUMN:`` O valor da coluna da tabela relacional que está sendo inserida ou atualizada.
- ``name:`` O nome da coluna da tabela relacional que está sendo inserida ou atualizada.
- ``<![CDATA[conteúdo a ser adicionado]]>``


Campos da Tabela Relacional FASE
--------------------------------

- ``Data Jornada:`` Tipo Data [Data de atualização no sistema]
- ``Data Pagto:`` Tipo Data [Data que efetivou a compra]
- ``Email Fase:`` Tipo Text [Email do cliente]
- ``Fase Jornada:`` Tipo Text [Fase Compra ou Fase Cliente]
- ``Status Pagto:`` Tipo Sim/Não
- ``Tipo Pagto:`` Tipo Text [Tipo Boleto ou Cartão]

Fazendo o Insert na Tabela Relacional
-------------------------------------

Faça a chamada SESSIONID http://api1.silverpop.com/XMLAPI?jsessionid=18CB5F81EA52FF880DC49514EA7BEC97

Passe os parâmetros abaixo:

.. code-block:: html

        <Envelope>
          <Body>
            <InsertUpdateRelationalTable>
              <TABLE_ID>11072</TABLE_ID>
              <ROWS>
                <ROW>
                  <COLUMN name="Data Jornada">
                    <![CDATA[data]]>
                  </COLUMN>
                  <COLUMN name="Data Pagto">
                    <![CDATA[data]]>
                  </COLUMN>
                  <COLUMN name="Email Fase">
                    <![CDATA[email]]>
                  </COLUMN>
                  <COLUMN name="Fase Jornada">
                    <![CDATA[cliente ou compra]]>
                  </COLUMN>
                  <COLUMN name="Status Pagto">
                    <![CDATA[sim ou não]]>
                  </COLUMN>
                  <COLUMN name="Tipo Pagto">
                    <![CDATA[boleto ou cartão]]>
                  </COLUMN>
                  <COLUMN name="idFase">
                    <![CDATA[id gerado automaticamente]]>
                  </COLUMN>
                </ROW>
              </ROWS>
            </InsertUpdateRelationalTable>
          </Body>
        </Envelope>

.. _fazendo-o-insert-na-tabela-principal:


Tabela Principal - Overview
---------------------------

A tabela principal e onde irá armazenar os dados cadastrais do usuário, abaixo seguem os campos da tabela.

- ``Email:`` O ID da Tabela de Automação de Campanha do Watson que deve ser atualizada.
- ``Area Atuacao:`` O ID da Tabela de Automação de Campanha do Watson que deve ser atualizada.
- ``Bairro:`` Nó XML que contém elementos ROW para cada linha que está sendo inserida ou atualizada.
- ``Celular:`` Nós XML que definem uma linha de tabela relacional e suas colunas.
- ``CEP:`` O valor da coluna da tabela relacional que está sendo inserida ou atualizada.
- ``Cidade:`` O nome da coluna da tabela relacional que está sendo inserida ou atualizada.
- ``CPF CNPJ:`` O ID da Tabela de Automação de Campanha do Watson que deve ser atualizada.
- ``Data Compra Consulta:`` Nó XML que contém elementos ROW para cada linha que está sendo inserida ou atualizada.
- ``DDD Celular:`` Nós XML que definem uma linha de tabela relacional e suas colunas.
- ``DDD Telefone:`` O valor da coluna da tabela relacional que está sendo inserida ou atualizada.
- ``End Complemento:`` O nome da coluna da tabela relacional que está sendo inserida ou atualizada.
- ``End Numero:`` Nós XML que definem uma linha de tabela relacional e suas colunas.
- ``Endereco:`` O valor da coluna da tabela relacional que está sendo inserida ou atualizada.
- ``Estado:`` O nome da coluna da tabela relacional que está sendo inserida ou atualizada.
- ``Inscricao Estadual:`` Nós XML que definem uma linha de tabela relacional e suas colunas.
- ``Nome Razao Social:`` O valor da coluna da tabela relacional que está sendo inserida ou atualizada.
- ``Placa:`` O nome da coluna da tabela relacional que está sendo inserida ou atualizada.
- ``Qtd Compra Consulta:`` Nós XML que definem uma linha de tabela relacional e suas colunas.
- ``Telefone:`` O valor da coluna da tabela relacional que está sendo inserida ou atualizada.
- ``Tipo Pessoa:`` O nome da coluna da tabela relacional que está sendo inserida ou atualizada.

ID da Tabela Principal: ``10804``



