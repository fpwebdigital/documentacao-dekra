.. index:: abi, application binary interface

.. _ABI:

************************************
Fazendo o Insert na Tabela Principal
************************************


Basic Design
============


Esse documento servirá de base para a equipe Técnica fazer os acessos e as requisições via API no sistema IBM Campaign Automation.

Overview da API do Watson Campaign Automation: `https://developer.ibm.com/customer-engagement/docs/watson-marketing/ibm-engage-2/watson-campaign-automation-platform/xml-api/api-xml-overview/ <https://developer.ibm.com/customer-engagement/docs/watson-marketing/ibm-engage-2/watson-campaign-automation-platform/xml-api/api-xml-overview/>`_.


Login de Usuário
================

Antes de chamar qualquer operação, você deve obter um ID de sessão usando a operação Login como mostrado abaixo.

Deverá fazer uma requisição ``POST`` no seguinte endereço: http://api9.silverpop.com/XMLAPI

Feito isso precisa passar os parâmetros abaixo para receber de volta o ``SESSIONID``  que será usado para fazer a conexão da API.
